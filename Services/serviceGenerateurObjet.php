<?php

function code_aleatoire(&$donnees){
    $cpt=1;
    $code_aleatoire = rand(100000,999999);
    $fr = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"),0,2);
    $vrai_code = $fr.$code_aleatoire;
    while($cpt!=0){
        $cpt=0;
        foreach($donnees as $valeur){
            if ($vrai_code == $valeur){
                $code_aleatoire = rand(100000,999999);
                $fr = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"),0,2);
                $vrai_code = $fr.$code_aleatoire;
                $cpt=1;
            }
        }
        if($cpt==0){
            $donnees[]=$vrai_code;
            return $vrai_code;
        }
    }
}

function numero_aleatoire(&$donnees){
        $cpt=0;
        $cpt2=1;
        $idPersonne=rand(10000000000,99999999999);
        while($cpt2!=0){
            $cpt2=0;
            foreach($donnees as $valeur){
                if ($idPersonne == $valeur){
                    $idPersonne=rand(100000000000,99999999999);
                    $cpt2=1;
                }
            }
            if($cpt==0){
                $donnees1[]=$idPersonne;
            }
        }
        return $idPersonne;
}
//$idPersonne=str_pad($cpt++, 6,0, STR_PAD_LEFT);

function code_agence(&$donnees3){
    $cpt=1;
        $numero_aleatoire = rand(100,999);
        while($cpt!=0){
            $cpt=0;
            foreach($donnees3 as $valeur2){
                if ($numero_aleatoire == $valeur2){
                    $numero_aleatoire = rand(100,999);
                    $cpt=1;
                }
            }
            if($cpt==0){
                $donnees3[]=$numero_aleatoire;
            }
        }
        return $numero_aleatoire;
}
if (($handle = fopen("./CSV/infos_clients.csv", "a+")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
           $client["nom"]= $data[0];
           $client["prenom"]=$data[1];
           $client["naissance"]= $data[2];
           $client["identifiant"]=$data[3];
           $donnees[]=$client["identifiant"];
           $client["mail"]= $data[4];
           $client["decouvert"]=$data[5];
           $clients[]= new Client( $client["nom"],
        $client["prenom"],
        $client["naissance"],
        $client["identifiant"],
        $client["mail"],
        $client["decouvert"]);
    }
    fclose($handle);
}
if (($handle = fopen("./CSV/infos_agences.csv", "a+")) !== FALSE) {
    while (($data2 = fgetcsv($handle, 1000, ";")) !== FALSE) {
           $agence["nom"]= $data2[0];
           $agence["code"]=$data2[1];
           $donnees3[]=$agence["code"];
           $agence["adresse"]= $data2[2];
           $agence["postal"]=$data2[3];
           $agences[]=new Agence($agence["nom"], $agence["code"], $agence["adresse"], $agence["postal"]);
    }
    fclose($handle);
}
if (($handle = fopen("./CSV/infos_comptes.csv", "a+")) !== FALSE) {
    while (($data1 = fgetcsv($handle, 1000, ";")) !== FALSE) {
           $compte["codeagence"]= $data1[0];
           $compte["numero"]=$data1[1];
           $donnees[]=$compte["numero"];
           $compte["identifiant"]= $data1[2];
           $compte["solde"]=$data1[3];
           $compte["type"]=$data1[4];
           $comptes[]=new Compte($compte["codeagence"],$compte["numero"],$compte["identifiant"],$compte["solde"],$compte["type"]);
    }
    fclose($handle);
}
if (($handle = fopen("./CSV/infos_administrateur.csv", "a+")) !== FALSE) {
    while (($data1 = fgetcsv($handle, 1000, ";")) !== FALSE) {
           $compte["nom"]= $data1[0];
           $compte["prenom"]=$data1[1];
           $compte["login"]= $data1[2];
           $loginAdministrateur[]=$compte["login"];
           $compte["motDePass"]=$data1[3];
           $administrateurs[]=new Administrateur($compte["nom"],$compte["prenom"],$compte["login"],$compte["motDePass"]);
    }
    fclose($handle);
}
if (($handle = fopen("./CSV/infos_conseiller.csv", "a+")) !== FALSE) {
    while (($data1 = fgetcsv($handle, 1000, ";")) !== FALSE) {
           $compte["nom"]= $data1[0];
           $compte["prenom"]=$data1[1];
           $compte["login"]= $data1[2];
           $loginConseiller[]=$compte["login"];
           $compte["motDePass"]=$data1[3];
           $administrateurs[]=new Conseiller($compte["nom"],$compte["prenom"],$compte["login"],$compte["motDePass"]);
    }
    fclose($handle);
}

