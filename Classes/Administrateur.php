<?php



class Administrateur
{
    private ?string $nom;
    private ?string $prenom;
    private ?string $login;
    private ?string $motDepass;

    public function __construct(string $nom, string $prenom, $login, string $motDepass)
    {
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->login = $login;
        $this->motDepass = $motDepass;
    }


    /**
     * Get the value of nom
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * Get the value of prenom
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set the value of prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * Get the value of login
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set the value of login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * Get the value of motDepass
     */
    public function getMotDepass()
    {
        return $this->motDepass;
    }

    /**
     * Set the value of motDepass
     */
    public function setMotDepass($motDepass)
    {
        $this->motDepass = $motDepass;
    }
}
