<?php

class Compte
{
    private int $codeAgence;
    private int $compteNumero;
    private string $compteId;
    private float $solde;
    private string $type;
    private bool $estActive=false;

    public function __construct(int $codeAgence, int $compteNumero, string $compteId, float $solde, string $type)
    {
        $this->codeAgence = $codeAgence;
        $this->compteNumero = $compteNumero;
        $this->compteId = $compteId;
        $this->solde = $solde;
        $this->type = $type;
        $this->estActive = false;
    }
    /**
     * Get the value of codeAgence
     */ 
    public function getCompteCodeAgence()
    {
        return $this->codeAgence;
    }

    /**
     * Set the value of codeAgence
     *
     * @return  self
     */ 
    public function setCompteCodeAgence($codeAgence)
    {
        $this->codeAgence = $codeAgence;
    }

    /**
     * Get the value of compteNumero
     */ 
    public function getNumero()
    {
        return $this->compteNumero;
    }

    /**
     * Set the value of compteNumero
     *
     * @return  self
     */ 
    public function setNumero($compteNumero)
    {
        $this->compteNumero = $compteNumero;
    }

    /**
     * Get the value of compteIdentifiant
     */ 
    public function getId()
    {
        return $this->compteId;
    }

    /**
     * Set the value of compteIdentifiant
     *
     * @return  self
     */ 
    public function setId($compteId)
    {
        $this->compteId = $compteId;
    }

    /**
     * Get the value of solde
     */ 
    public function getSolde()
    {
        return $this->solde;
    }

    /**
     * Set the value of solde
     *
     * @return  self
     */ 
    public function setSolde($solde)
    {
        $this->solde = $solde;
    }

    /**
     * Get the value of type
     */ 
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of type
     *
     * @return  self
     */ 
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get the value of estActive
     */ 
    public function getEstActive()
    {
        return $this->estActive;
    }

    /**
     * Set the value of estActive
     */ 
    public function setEstActive($estActive)
    {
        $this->estActive = $estActive;
    }
}
