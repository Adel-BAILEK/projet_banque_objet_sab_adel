<?php



class Client
{
    private string $nom;
    private string $prenom;
    private string $naissance;
    private string $id;
    private string $mail;
    private string $decouvert;
    private bool $estActive = false;

    public function __construct(string $nom, string $prenom, string $naissance, string $id, string $mail, string $decouvert)
    {
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->naissance = $naissance;
        $this->id = $id;
        $this->mail = $mail;
        $this->decouvert = $decouvert;
        $this->estActive = false;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the value of nom
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * Get the value of prenom
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set the value of prenom
     *
     * @return  self
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * Get the value of naissance
     */
    public function getNaissance()
    {
        return $this->naissance;
    }

    /**
     * Set the value of naissance
     *
     * @return  self
     */
    public function setNaissance($naissance)
    {
        $this->naissance = $naissance;
    }

    /**
     * Get the value of mail
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set the value of mail
     *
     * @return  self
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * Get the value of decouvert
     */
    public function getDecouvert()
    {
        return $this->decouvert;
    }

    /**
     * Set the value of decouvert
     *
     * @return  self
     */
    public function setDecouvert($decouvert)
    {
        $this->decouvert = $decouvert;
    }

    /**
     * Get the value of estActive
     */
    public function getEstActive()
    {
        return $this->estActive;
    }

    /**
     * Set the value of estActive
     */
    public function setEstActive($estActive)
    {
        $this->estActive = $estActive;
    }
}
